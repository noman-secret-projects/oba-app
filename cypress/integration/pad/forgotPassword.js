describe("forgotPasword", () => {
    beforeEach(() => {
        //Go to the specified URL
        cy.visit("http://localhost:8080");
    });

    const mail = "ryanbaab1@gmail.com";
    const badMail = "Mail@gmail.com";

    it('Validate forgot password form', function () {
        cy.get(".btn-2").should("exist").click();

        cy.get("#modalInputEmail").should("exist");
        cy.get("#sendMail").should("exist");
        cy.get(".close").should("exist");
    });

    //this is used to recover your password.
    it('Forgot Password succesful', function () {
        cy.server();
        cy.route("POST", "/user", {"email": mail}).as("checkMail");
        cy.route("POST", "/user/send", {"mail": mail}).as("sendMail");

        //wait otherwise it won't type the whole mail
        cy.get(".btn-2").click().wait(500);

        cy.get("#modalInputEmail").type(mail);

        cy.get("#sendMail").click();

        cy.wait("@checkMail");
        cy.get("@checkMail").should((xhr) => {
            expect(xhr.request.body.email).equals(mail);
        });

        cy.wait("@sendMail");
        cy.get("@sendMail").should((xhr) => {
            expect(xhr.request.body.email).equals(mail);
        });

        //if this shows than the mail is send
        cy.get(".text-success").should("exist");
    });

    it('Forgot Password, empty', function () {
        cy.get(".btn-2").click().wait(500);

        cy.get("#sendMail").click();

        cy.get(".modal-error").should("contain", "Voer uw mail in!")
    });

    it('Forgot Password, not a registered mail', function () {
        cy.server();
        cy.route("POST", "/user", {reason: "ERROR", status: 400}).as("checkMail");

        //wait otherwise it won't type the whole mail
        cy.get(".btn-2").click().wait(500);
        cy.get("#modalInputEmail").type(badMail);

        cy.get("#sendMail").click();

        cy.wait("@checkMail");
        cy.get("@checkMail").should((xhr) => {
            expect(xhr.request.body.email).equals(badMail);
        });

        cy.get(".modal-error").should("contain", "Deze email is nog niet geregistreerd")
    });
});
