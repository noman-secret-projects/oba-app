describe("Link NFC Tag", () => {
    const EMAIL = "email@test.com";
    const PASSWORD = "12345678";
    const EVENT_ID = "3210";
    const ROOM = "1"

    //Run before each test in this context
    beforeEach(() => {
        cy.visit("http://localhost:8080#register");

        cy.server();

        cy.route("POST", "/user/register", {"status": 200}).as("register");
        cy.route("POST", "/user/login", {"email": EMAIL}).as("login");
        cy.route("POST", "/user", {"email": EMAIL}).as("user");

        cy.get("#email").type(EMAIL);
        cy.get("#password").type(PASSWORD);
        cy.get("#passwordConfirm").type(PASSWORD);

        cy.get("form.register-form > button[type=submit]").click();

        cy.wait("@register");

        cy.get("@register").should((xhr) => {
            expect(xhr.request.body.email).equals(EMAIL);
            expect(xhr.request.body.password).equals(PASSWORD);
        });

        cy.wait("@login");
        cy.get("@login").should((xhr) => {
            expect(xhr.request.body.email).equals(EMAIL);
            expect(xhr.request.body.password).equals(PASSWORD);
        });
    });


    it("test redirect if there's no id", () => {

        cy.visit("http://localhost:8080#nfc");

        cy.reload()

        cy.url().should("contain", "#event");

    })

    it("check if cookie is created", () => {

        cy.visit("http://localhost:8080/?id=" + EVENT_ID + "#nfc");
        cy.reload()

        cy.wait(100)

        cy.url().should("contain", "#nfc");
        cy.getCookie('event_to_nfc').should('have.property', 'value', EVENT_ID)
    })

    it("check if nfc tag is linked", () => {

        cy.visit("http://localhost:8080/?id=" + EVENT_ID + "#nfc");
        cy.reload()

        cy.wait(100)

        cy.url().should("contain", "#nfc");
        cy.getCookie('event_to_nfc').should('have.property', 'value', EVENT_ID)

        cy.visit("http://localhost:8080/?loc=" + ROOM);
        cy.reload()

        cy.url().should("contain", "#nfc_counted");

        cy.route({
            method: 'GET',
            url: 'room/get/eventId',
            status: 200,
            response: {
                rolesCount: 2
            },
        })
    })
})