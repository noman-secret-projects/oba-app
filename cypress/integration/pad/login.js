//Context: Login
describe("Login", () => {
    //Run before each test in this context
    beforeEach(() => {
        //Go to the specified URL
        cy.visit("http://localhost:8080");
    });

    const mail = "ryanbaab1@gmail.com";
    const password = "testtest";
    const badPassword = "WrongPassword123";

    //checks if all the buttons are still there
    it('Validate login form ', function () {
        cy.get("#InputEmail").should("exist");
        cy.get("#InputPassword").should("exist");
        cy.get("button[type=submit]").should("exist");
        cy.get("button[type=button]").should("exist");
    });

    //tries to make a succesful login
    it('Successful login', function () {
        cy.server();
        cy.route("POST", "/user/login", {"email": "ryanbaab1@gmail.com"}).as("login");

        cy.get("#InputEmail").type(mail);
        cy.get("#InputPassword").type(password);
        cy.get("button[type=submit]").click();

        cy.wait("@login");
        cy.get("@login").should((xhr) => {
            expect(xhr.request.body.email).equals(mail);
            expect(xhr.request.body.password).equals(password);
        });

        //if the url turns into #events than the login was successful
        cy.url().should("contain", "#events");
    });

    //its a failed login
    it('Failed login', function () {
        cy.server();
        cy.route({method: "POST", url: "/user/login", response: {reason: "ERROR"}, status: 401}).as("login");

        cy.get("#InputEmail").type(mail);
        cy.get("#InputPassword").type(badPassword);
        cy.get("button[type=submit]").click();

        cy.wait("@login");
        cy.get("@login").should((xhr) => {
            expect(xhr.request.body.email).equals(mail);
            expect(xhr.request.body.password).equals(badPassword);
        });

        //if this shows than the login has failed
        cy.get(".error").should("contain", "ERROR");
    });
});