describe("Location", () => {

    //Run before each test in this context
    beforeEach(() => {
        //Go to the specified URL
        cy.visit("http://localhost:8080#");

        cy.server();
        cy.route("POST", "/user/login", {"email": "sahil.mankani@hva.nl"}).as("login");

        cy.get("#InputEmail").type("sahil.mankani@hva.nl");
        cy.get("#InputPassword").type("1234567890");
        cy.get("button[type=submit]").click().wait(2000);


    });

    it('get location',()=>{
        cy.visit("http://localhost:8080#location");
        cy.get('.mapboxgl-ctrl-geolocate').click()

    })

    it('get event base on selected location',()=>{

    })

})