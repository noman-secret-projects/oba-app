/**
 * Unit test for editing account info
 *
 * @author Adam Abbas
 */

describe("account", () => {
    const OLD_EMAIL = "email@test.com";
    const OLD_PASSWORD = "12345678";
    const INVALID_PASSWORD = "123456";
    const NEW_PASSWORD = "123456789";
    const NEW_EMAIL = "email3@test.com";
    const ALREADY_EXISTING_EMAIL = "email2@test.com";
    const ALREADY_EXISTING_PASSWORD = "88888888";

    beforeEach(() => {
        cy.visit("http://localhost:8080#register");

        cy.server();

        cy.route("POST", "/user/register", {"status": 200}).as("register");
        cy.route("POST", "/user/login", {"email": OLD_EMAIL}).as("login");
        cy.route("POST", "/user", {"email": OLD_EMAIL}).as("user");

        cy.get("#email").type(OLD_EMAIL);
        cy.get("#password").type(OLD_PASSWORD);
        cy.get("#passwordConfirm").type(OLD_PASSWORD);

        cy.get("form.register-form > button[type=submit]").click();

        cy.wait("@register");

        cy.get("@register").should((xhr) => {
            expect(xhr.request.body.email).equals(OLD_EMAIL);
            expect(xhr.request.body.password).equals(OLD_PASSWORD);
        });

        cy.wait("@login");
        cy.get("@login").should((xhr) => {
            expect(xhr.request.body.email).equals(OLD_EMAIL);
            expect(xhr.request.body.password).equals(OLD_PASSWORD);
        });
    });

    it('Validate account form', () => {
        //After a successful register (and login), the URL should now contain #welcome.
        cy.url().should("contain", "#account");
        cy.get("#editbtn").should("exist");

        cy.get("#editbtn").click();

        cy.get("#email").should("exist");
        cy.get("#old-password").should("exist");
        cy.get("#new-password").should("exist");
        cy.get("#confirm-new-password").should("exist");
        cy.get("#confirmbtn").should("exist");
        cy.get("#cancelbtn").should("exist");
    });

    it('Invalid email input', () => {
        cy.url().should("contain", "#account");
        cy.get("#editbtn").click();

        cy.get("#email").clear().type("testest");
        cy.get("#confirmbtn").click();

        cy.get("#error").should("contain", "Voer een correct e-mailadres in!");
    });

    it('Wrong old-password input', () => {
        cy.server();
        cy.route({method: "POST", url: "/user/login", response: {reason: "Verkeerd email of wachtwoord"}, status: 401}).as("login");

        cy.url().should("contain", "#account");
        cy.get("#editbtn").click();

        cy.get("#old-password").type(ALREADY_EXISTING_PASSWORD);
        cy.get("#confirmbtn").click();

        cy.get("@login").should((xhr) => {
            expect(xhr.request.body.email).equals(OLD_EMAIL);
            expect(xhr.request.body.password).equals(ALREADY_EXISTING_PASSWORD);
        });

        cy.get("#error").should("contain", "Oud wachtwoord is onjuist!");
    });

    it('Invalid new-password input', () => {
        cy.url().should("contain", "#account");
        cy.get("#editbtn").click();

        cy.get("#new-password").type(INVALID_PASSWORD);
        cy.get("#confirmbtn").click();

        cy.get("#error").should("contain", "Wachtwoord moet minimaal 8 karakters zijn!");
    });

    it("New password confirm doesn't match" , () => {
        cy.url().should("contain", "#account");
        cy.get("#editbtn").click();

        cy.get("#new-password").type(NEW_PASSWORD);
        cy.get("#confirm-new-password").type("12345678");
        cy.get("#confirmbtn").click();

        cy.get("#error").should("contain", "Het bevestigde wachtwoord komt niet overeen!");
    });

    it("Succesfull password change", () => {
        cy.url().should("contain", "#account");
        cy.get("#editbtn").click();
        cy.server();
        cy.route("POST", "/user/update/password", {"email": "email@test.com", "password": "123456789"}).as("updatePassword");

        cy.get("#old-password").type(OLD_PASSWORD);
        cy.get("#new-password").type(NEW_PASSWORD);
        cy.get("#confirm-new-password").type(NEW_PASSWORD);
        cy.get("#confirmbtn").click();

        cy.get("@updatePassword").should((xhr) => {
            expect(xhr.request.body.email).equals(OLD_EMAIL);
            expect(xhr.request.body.password).equals(NEW_PASSWORD);
        });
    });

    it("E-mail already exists", () => {
        cy.url().should("contain", "#account");
        cy.get("#editbtn").click();

        cy.server();
        cy.route({method: "POST", url: "/user/update", response: {code: 400}, status: 200}).as("invalidEmail");

        cy.get("#email").clear().type(ALREADY_EXISTING_EMAIL);
        cy.get("#confirmbtn").click();

        cy.get("#error").should("contain", "Het e-mail is al in gebruik!")
    });

    it("Succesfull email change", () => {
        cy.url().should("contain", "#account");
        cy.get("#editbtn").click();

        cy.server();
        cy.route("POST", "/user/update", {"newemail": NEW_EMAIL, "oldemail": OLD_EMAIL}).as("emailChange");
        cy.route("POST", "/user", {"email": "email3@test.com"}).as("user")
        cy.wait("@user");

        cy.get("#email").clear().type(NEW_EMAIL);
        cy.get("#confirmbtn").click();

        cy.get("@emailChange").should((xhr) => {
            expect(xhr.request.body.oldemail).equals(OLD_EMAIL);
            expect(xhr.request.body.newemail).equals(NEW_EMAIL);
        });

        cy.get("#name").should("contain", NEW_EMAIL)
    });
});