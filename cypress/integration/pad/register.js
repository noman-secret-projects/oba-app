describe("Register", () => {

     //Run before each test in this context
     beforeEach(() => {
        //Go to the specified URL
        cy.visit("http://localhost:8080#register");
    });


    it("validate register form", () => {

        cy.get("#email").should("exist");
        cy.get("#password").should("exist");
        cy.get("#passwordConfirm").should("exist");
        cy.get("form.register-form > button[type=submit]").should("exist")
    })

    it("Successfull register", () => {

        cy.server();

        cy.route("POST", "/user/register", {"status": 200}).as("register");
        cy.route("POST", "/user/login", {"email": "email@test.com"}).as("login");

        cy.get("#email").type("test@test.com");
        cy.get("#password").type("12345678");
        cy.get("#passwordConfirm").type("12345678");

        cy.get("form.register-form > button[type=submit]").click();

        cy.wait("@register");

        cy.get("@register").should((xhr) => {
            expect(xhr.request.body.email).equals("test@test.com");
            expect(xhr.request.body.password).equals("12345678");
        });

        cy.wait("@login");
        cy.get("@login").should((xhr) => {
            expect(xhr.request.body.email).equals("test@test.com");
            expect(xhr.request.body.password).equals("12345678");
        });

        //After a successful register (and login), the URL should now contain #welcome.
        cy.url().should("contain", "#account");

    })

    it("Failed register [user already exists]", function () {
        //Start a fake server
        cy.server();

        //Add a stub with the URL /user/register as a POST
        //Respond with a JSON-object when requested and set the status-code tot 401.
        //Give the stub the alias: @register
        cy.route({
            method: "POST",
            url: "/user/register",
            response: {
                reason: {
                    code: "ER_DUP_ENTRY"
                }
            },
            status: 401
        }).as("register");

        //Find the field for the email and type the text "noman.jabbar@hva.nl".
        cy.get("#email").type("noman.jabbar@hva.nl");

        //Find the field for the password and type the text "12345678".
        cy.get("#password").type("12345678");

        cy.get("#passwordConfirm").type("12345678");

        //Find the button to login and click it.
        cy.get("form.register-form > button[type=submit]").click();

        //Wait for the @register-stub to be called by the click-event.
        cy.wait("@register");

        //After a failed register, an element containing our error-message should be shown.
        cy.get(".error > p").should("exist").should("contain", "Dit emailadres is al in gebruik!");
    });

    it("Failed register [password length cannot be lower than 8 characters]", function () {
        //Start a fake server
        cy.server();

        //Add a stub with the URL /user/register as a POST
        //Respond with a JSON-object when requested and set the status-code tot 401.
        //Give the stub the alias: @register

        //Find the field for the email and type the text "noman.jabbar@hva.nl".
        cy.get("#email").type("noman.jabbar@hva.nl");

        //Find the field for the password and type the text "1234567".
        cy.get("#password").type("1234567");

        cy.get("#passwordConfirm").type("1234567");

        //Find the button to login and click it.
        cy.get("form.register-form > button[type=submit]").click();

        //After a failed register, an element containing our error-message should be shown.
        cy.get(".error > p").should("exist").should("contain", "Het wachtwoord mag niet kleiner dan 8 karakters zijn.");
    });

    it("Failed register [passwords do not match]", function () {
        //Start a fake server
        cy.server();

        //Find the field for the email and type the text "noman.jabbar@hva.nl".
        cy.get("#email").type("noman.jabbar@hva.nl");

        //Find the field for the password and type the text "12345678".
        cy.get("#password").type("12345678");

        cy.get("#passwordConfirm").type("1234567");

        //Find the button to login and click it.
        cy.get("form.register-form > button[type=submit]").click();

        //After a failed register, an element containing our error-message should be shown.
        cy.get(".error > p").should("exist").should("contain", "De wachtwoorden komen niet overeen.");
    });

    it("Failed register [email is not a valid email adress]", function () {
        //Start a fake server
        cy.server();

        //Find the field for the email and type the text "noman.jabbar".
        cy.get("#email").invoke("attr", "type", "text");

        cy.get("#email").type("noman.jabbar");

        //Find the field for the password and type the text "12345678".
        cy.get("#password").type("12345678");

        cy.get("#passwordConfirm").type("1234567");

        //Find the button to login and click it.
        cy.get("form.register-form > button[type=submit]").click();

        //After a failed register, an element containing our error-message should be shown.
        cy.get(".error > p").should("exist").should("contain", "Dit is geen emailadres.");
    });
})