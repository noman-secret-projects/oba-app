/**
 * Entry point front end application - there is also an app.js for the backend (server folder)!
 *
 * Available: `sessionManager` or `networkManager` or `app.loadController(..)`
 *
 * You only want one instance of this class, therefore always use `app`.
 *
 * @author Noman Jabbar, Sam Overheul, Adam Abbas, Ryan Veerman & Sahil Mankani.
 */
const CONTROLLER_SIDEBAR = "sidebar";
const CONTROLLER_LOGIN = "login";
const CONTROLLER_REGISTER = "register";
const CONTROLLER_LOGOUT = "logout";
const CONTROLLER_WELCOME = "welcome";
const CONTROLLER_ACCOUNT = "account";
const CONTROLLER_EDIT_ACCOUNT = "edit_account";
const CONTROLLER_EVENTS = "events";
const CONTROLLER_MANUAL_COUNT = "manual_count";
const CONTROLLER_PASS_RECOVERY = "password_recovery";
const CONTROLLER_LOCATION = "location";
const CONTROLLER_NFC_COUNTED = "nfc_counted";
const CONTROLLER_NFC = "nfc";
const CONTROLLER_DASHBOARD = "dashboard";

const sessionManager = new SessionManager();
const networkManager = new NetworkManager();

class App {

    init() {
        //Always load the sidebar
        this.loadController(CONTROLLER_SIDEBAR);
        //Attempt to load the controller from the URL, if it fails, fall back to the welcome controller.
        this.loadControllerFromUrl(CONTROLLER_LOGIN);
    }

    /**
     * Loads a controller
     * @param name - name of controller - see constants
     * @param controllerData - data to pass from on controller to another
     * @returns {boolean} - successful controller change
     */
    loadController(name, controllerData) {
        this.name = name;
        //uncomment line below for debugging.
        // console.log("loadController: " + this.name);

        // if there is controllerData, parse the data to the url like a url parameter.
        if (controllerData) {
            this.parseData(controllerData, name);
        } else {
            controllerData = {};
        }

        switch (this.name) {
            case CONTROLLER_LOGOUT:
                this.setCurrentController(this.name);
                this.handleLogout();
                break;

            case CONTROLLER_PASS_RECOVERY:
                new PassRecoveryController();
                break;

            case CONTROLLER_SIDEBAR:
                new NavbarController();
                break;

            case CONTROLLER_EDIT_ACCOUNT:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new EditAccountController(), () => new LoginController(), () => new NfcCountedController());
                break;

            case CONTROLLER_LOGIN:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new EventsController(), () => new LoginController(), () => new NfcCountedController());
                break;
            case CONTROLLER_ACCOUNT:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new AccountController(), () => new LoginController(), () => new NfcCountedController());
                break;
            case CONTROLLER_REGISTER:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new EventsController(), () => new RegisterController(), () => new NfcCountedController());
                break;

            case CONTROLLER_WELCOME:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new WelcomeController(), () => new LoginController(), () => new NfcCountedController());
                break;

            case CONTROLLER_MANUAL_COUNT:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new ManualCountController(), () => new LoginController(), () => new NfcCountedController());
                break;

            case CONTROLLER_EVENTS:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new EventsController(), () => new LoginController(), () => new NfcCountedController());
                break;

            case CONTROLLER_LOCATION:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new LocationController(), () => new LoginController(), () => new NfcCountedController());
                break;

            case CONTROLLER_DASHBOARD:
                this.setCurrentController(this.name);
                if (networkManager.urlParametersCheckNFC()) {
                    new NfcCountedController();
                } else {
                    this.isLoggedIn(() => new DashboardController(), () => new LoginController());
                }
                break;

            case CONTROLLER_NFC_COUNTED:
                this.setCurrentController(this.name);
                new NfcCountedController();
                break;

            case CONTROLLER_NFC:
                this.setCurrentController(this.name);
                this.isLoggedIn(() => new NfcController(), () => new LoginController());
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Alternative way of loading controller by url
     * @param fallbackController
     */
    loadControllerFromUrl(fallbackController) {
        const currentController = this.getCurrentController();
        if (currentController) {
            if (!this.loadController(currentController)) {
                this.loadController(fallbackController);
            }
        } else {
            this.loadController(fallbackController);
        }
    }

    /**
     *  create the parameters for the url.
     * @param {Object} data
     * @param {String} name
     */
    parseData(data, name) {
        let parseData = "";

        // foreach key and value, stringify it and eventually add it to the url as parameters.
        for (let [key, value] of Object.entries(data)) {
            parseData = "";
            parseData += key + "=" + value + "&";
        }
        this.setCurrentController(name);
        location.search = parseData;

    }

    /**
     * @returns the name of the current controller.
     */
    getCurrentController() {
        return location.hash.slice(1);
    }

    /**
     * @description set the current controller with a name.
     * @param {String} name
     */
    setCurrentController(name) {
        location.hash = name;
    }

    /**
     * @description Convenience functions to handle logged-in states
     * @param whenYes - function to execute when user is logged in
     * @param whenNo - function to execute when user is logged in
     * @param whenParamLoc - function that returns the NFC constroller if the loc url parameter is present.
     * @returns {CallableFunction}
     */
    isLoggedIn(whenYes, whenNo, whenParamLoc) {

        if(networkManager.urlParametersCheckNFC()) {
            whenParamLoc();
        }else {
            if(sessionManager.get("email")) {
                whenYes();
            } else {
                whenNo();
            }
        }

    }

    /**
     * Removes email via sessionManager and loads the login screen
     */
    handleLogout() {
        // sessionManager.remove("email");
        sessionManager.clear();
        //go to login screen
        this.loadController(CONTROLLER_LOGIN);
    }
}

const app = new App();
//When the DOM is ready, kick off our application.
$(function () {
    app.init();
});