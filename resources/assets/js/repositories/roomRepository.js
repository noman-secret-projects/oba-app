/**
 * Repository responsible for all room data from server - Count
 *
 * @author Sam Overheul
 */
class RoomRepository {

    constructor() {
        this.route = "/room"
    }
    /**
     * async function that handles a count.
     * @param {Number} eventId 
     * @returns {Promise<unkown>}
     */
    async addCount(eventId) {
        return await networkManager.doRequest(
            `${this.route}/set/eventId`, {"eventId": eventId}
        );
    }

    /**
     * Links a NFC tag to an event
     * @param roomId - number for a room, if it isn't available it will be created
     * @param timestamp - time in milliseconds, timestamp tells at which time "link" will be invalid
     * @param eventId - id for the different events
     * @returns {Promise<unkown>}
     */
    async setNfcTag(roomId, timestamp, eventId) {
        return await networkManager.doRequest(
            `${this.route}/set/tag`, {"roomId": roomId, "timestamp": timestamp, "eventId": eventId}
        );
    }

    /**
     * Returns an event ID linked to time and room ID
     * @param roomId - number for a room, if it isn't available it will be created
     * @param timestamp - time in milliseconds, timestamp tells at which time "link" will be invalid
     * @returns {Promise<unkown>}
     */
    async getEvent(roomId, timestamp) {
        return await networkManager.doRequest(
            `${this.route}/get/eventId`, {"roomId": roomId, "timestamp": timestamp}
        );
    }
}