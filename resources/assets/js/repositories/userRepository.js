/**
 * Repository responsible for all user data from server - CRUD
 * Make sure all functions are using the async keyword when interacting with network!
 *
 * @author Noman Jabbar, Sam Overheul, Adam Abbas, Ryan Veerman & Sahil Mankani.
 */
class UserRepository {

    constructor() {
        this.route = "/user"
    }
    /**
     * @description async function to get the user by his/her email. 
     * @param {String} email
     * @returns {Promise<unknown>}
     */
    async getUser(email) {
        return await networkManager
            .doRequest(`${this.route}`, {"email": email});
    }
    /**
     * @description async function to update a user by his/her email.
     * @param {String} newemail 
     * @param {String} oldemail 
     * @returns {Promise<unknown>}
     */
    async updateUser(newemail, oldemail) {
        return await networkManager
            .doRequest(`${this.route}/update`, {"newemail": newemail, "oldemail": oldemail});
    }
    /**
     * @description async function to update a users password.
     * @param {String} email 
     * @param {String} newPassword
     * @returns {Promise<unknown>} 
     */
    async updatePassword(email, newPassword) {
        return await networkManager
            .doRequest(`${this.route}/update/password`, {"email": email, "password": newPassword});
    }
    /**
     * @description async function that logins a user.
     * @param {String} email
     * @param {String} password
     * @returns {Promise<unknown>}
     */
    async login(email, password) {
        return await networkManager
            .doRequest(`${this.route}/login`, {"email": email, "password": password});
    }
    /**
     * @description async function that reguests a forgotten password mail.
     * @param {String} email
     * @returns {Promise<unknown>}
     */
    async forgotPassword(email) {
        return await networkManager
            .doRequest(`${this.route}/send`, {"email": email});
    }
    /**
     * @description async function that registers a user.
     * @param {String} email 
     * @param {String} password
     * @returns {Promise<unknown>}
     */
    async register(email, password) {
        return await networkManager
        .doRequest(`${this.route}/register`, {"email": email, "password": password});

    }
    /**
     * @description async function that verifies an email.
     * @param {Number} id
     * @param {SHA-256 Hash} hash
     * @returns {Promise<unknown>}
     */
    async verifyEmail(id, hash) {
        return await networkManager
        .doRequest(`${this.route}/verify/mail`, {"id": id, "hash": hash});
    }
}