/**
 * Repository responsible for all event data from server - CRUD
 *
 * @author Noman Jabbar, Adam Abbas & Ryan Veerman.
 */
class EventRepository {
    constructor() {
        this.route = "/event"
    }

    /**
     * @description async function that gets an event by the event id.
     * @param {Number} eventId
     * @returns {Promise<unknown>} 
     */
    async getEvent (eventId) {
        return await networkManager.doRequest(
            `${this.route}`, {"eventId": eventId}
        );
    }
    /**
     * @description async function that gets all events.
     * @returns {Promise<unknown>}
     */
    async getAllEvents () {
        return await networkManager.doRequest(
            `${this.route}/all`, {}
        );
    }
    /**
     * @description async function that gets the amount of an event.
     * @param {Number} eventId
     * @returns {Promise<unknown>} 
     */
    async getAmount(eventId) {
        return await networkManager.doRequest(
            `${this.route}/amount`, {"eventId": eventId}
        );
    }
    /**
     * @description async function that sets the amount of an event.
     * @param {Number} eventId
     * @param {Number} newAmount
     * @returns {Promise<unknown>} 
     */
    async setAmount(eventId, newAmount) {
        return await networkManager.doRequest(
            `${this.route}/update/amount`, {"eventId": eventId, "newAmount": newAmount}
        );
    }
    /**
     * @description async function that exports the events.
     * @returns {Promise<unknown>} 
     */
    async export (){
        return await networkManager.doRequest(
                `${this.route}/export`, {}
        );
    }
    /**
     * @description async function that gets the total number of events.
     * @returns {Promise<unknown>} 
     */
    async getTotalEvents() {
        return await networkManager.doRequest(
            `${this.route}/total`, {}
        );
    }
    /**
     * @description async function that gets the total number of participants.
     * @returns {Promise<unknown>} 
     */
    async getTotalParticipants() {
        return await networkManager.doRequest(
            `${this.route}/total/amount`, {}
        );
    }
    /**
     * @description async function that gets today's event title.
     * @param {Date} date
     * @returns {Promise<unknown>} 
     */
    async getTodaysEvent(date) {
        return await networkManager.doRequest(
            `${this.route}/today`, {"date": date}
        );
    }
    /**
     * @description async function that gets the last recent seven events.
     * @returns {Promise<unknown>} 
     */
    async getRecentEvents() {
        return await networkManager.doRequest(
            `${this.route}/recent`, {}
        );
    }
}