/**
 * @description Responsible for showing the events, filter options, export option and handling the count button on each event.
 *
 * known types of the events:
 * - id for identifying
 * - titles
 * - subjects
 * - course-infos
 * - course-prices
 * - course-tutors
 * - course-locations
 
 * @author Noman Jabbar, Adam Abbas, Sam Overheul & Ryan Veerman
 */
class EventsController extends Controller {
    constructor() {
        super();

        this.eventRepository = new EventRepository();

        $.get("views/events.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }

    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        app.loadController(CONTROLLER_SIDEBAR);
        //Load the welcome-content into memory
        this.eventsView = $(data);

        //variables for handling dropdowns. Declared true because they're hidden in the first place.
        this.sortDropDown = false;
        this.locationDropDown = false;
        this.filterDropDown = false;
        this.exportDropDown = false;

        this.eventsView.ready(() => {
            $("#loader").css('display', 'flex');
            this.fetchEvents();

        });
        // get all the events data
        $(".content").empty().append(this.eventsView);

        this.eventsView.find(".filterdropdownbtn").on("click", () => this.handleFilterDropDown());
        this.eventsView.find(".exportdropdownbtn").on("click", () => this.handleExportDropDown());
        this.eventsView.find("#exportbtn").on("click", () => this.export());

        this.eventsView.find(".dropdownItem").on("click", (event) => this.handleOrderOptionClick($(event.target).text()));
        this.eventsView.find("#delete-filters").on("click", () => this.handleDeleteFilters());
        this.eventsView.find(".location-dropdown").on("click", () => this.handleLocationDropDown());
        this.eventsView.find(".dropdownBtn").on("click", () => this.handleSortDropDown());

        this.eventsView.find(".dropdownItem").on("click", (event) => {
            this.handleOrderOptionClick($(event.target).text());
            this.sortDropDown = true;
            this.handleSortDropDown();
        });
    }
    /**
     * @description this function handles the export of the (filtered) events.
     */
    async export() {
        let data = this.filterData;
        let filteredEvents = [];

        if (data == null) {
            data = await this.eventRepository.export();
            data = data.data;
        }

        for (let i = 0; i < data.length; i++) {
            let date = data[i].date.split('T').shift();
            filteredEvents[i] = [data[i].id, data[i].title, date, data[i].time, data[i].amount];
        }

        let csv = 'id;title;date;time;amount;\n';
        filteredEvents.forEach((row) => {
            csv += row.join(';');
            csv += "\n";
        });

        let hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = 'evenementen.csv';

        hiddenElement.click();
    }
    /**
     * @description get all events from the repository.
     */
    async fetchEvents() {
        try {
            //await keyword 'stops' code until data is returned - can only be used in async function
            const events = await this.eventRepository.getAllEvents();

            this.eventsView.find(".page-title").prepend($("<span>", {
                class: "badge badge-dark events-total",
                text: Object.keys(events).length
            }));


            for (let key in events) {
                // create the layout for each event.
                this.createEventLayout(events[key]);
            }
            // if all events are shown, hide the loader.
            setTimeout(() => {
                $("#loader").css('display', 'none');
            }, 900);

            $("a.event-button").on("click", (event) => this.handleCountEvent(event));
            this.eventsView.find("#filterbtn").on("click", () => this.sortEvents());

        } catch (e) {
        }
    }
    /**
     * @description creates the layout of the events.
     * @param {Object} data 
     */
    createEventLayout(data) {
        $('.events-list').append(
            `<div class="event col-lg-6 col-sm-12" data-event-id="${data.id}">
            <h4 class="event-title">${data.title}</h4>
            <p class="event-details-btn" data-toggle="collapse" href="#eventDetails${data.id}" role="button" aria-expanded="false" aria-controls="eventDetails">Meer info</p>
            <ul id="eventDetails${data.id}" class="list-group list-group-flush collapse">
                <li class="list-group-item">${data.subject}</li>
                <li class="list-group-item">${data.info}</li>
                <li class="list-group-item">${data.location}</li>
                <li class="list-group-item">${data.tutor}</li>
                <li class="list-group-item">&euro; ${data.price}</li>
                <li class="list-group-item">Aantal personen: <span class="badge badge-info badge-pill">${data.amount}</span></li>
            </ul>
            <div class="event-button-wrapper">
                <a id="event_button" class="btn event-button" data-controller="welcome">Tellen</a>
            </div>
	    </div>`);
    }
    /**
     * @description handles the sort dropdown. 
     */
    handleSortDropDown() {
        if (this.sortDropDown) {
            this.sortDropDown = false;
            $(".dropdownBox").hide();
            $(".dropdownBtn").css("border-bottom", "1px solid #52585E");
        } else {
            this.sortDropDown = true;
            $(".dropdownBox").show();
            $(".dropdownBtn").css("border-bottom", "transparent");
        }
    }
    /**
     * @description handles the location dropdown. 
     */
    handleLocationDropDown() {
        if (this.locationDropDown) {
            this.locationDropDown = false;
            $('.location-box').slideUp();
            $('#location-fold').fadeOut("fast", () => {
                $('#location-dropdown').fadeIn("fast");
            });
        } else {
            this.locationDropDown = true;
            $('.location-box').slideDown();
            $('#location-dropdown').fadeOut("fast", () => {
                $('#location-fold').fadeIn("fast");
            });
        }
    }
    /**
     * @description handles the filter dropdown. 
     */
    handleFilterDropDown() {
        console.log(this.filterDropDown);
        if (this.filterDropDown) {
            this.filterDropDown = false;
            $('.filterbox').slideUp();
            $('#filterfoldbtn').fadeOut("fast", () => {
                $('#filterdropdownbtn').fadeIn(100);
            });
        } else {
            this.filterDropDown = true;
            $('.filterbox').slideDown();
            $('#filterdropdownbtn').fadeOut("fast", () => {
                $('#filterfoldbtn').fadeIn("fast");
            });
        }
    }
    /**
     * @description handles the export dropdown. 
     */
    handleExportDropDown() {
        if (this.exportDropDown) {
            this.exportDropDown = false;
            $('.exportbox').slideUp();
            $('#exportfoldbtn').fadeOut("fast", () => {
                $('#exportdropdownbtn').fadeIn(100);
            });
        } else {
            this.exportDropDown = true;
            $('.exportbox').slideDown();
            $('#exportdropdownbtn').fadeOut("fast", () => {
                $('#exportfoldbtn').fadeIn("fast");
            });
        }

    }
    /**
     * @description This function fires when an order by option is clicked.
     *
     * @param {String} value value of option
     */
    handleOrderOptionClick(value) {

        //gives an id for each option
        switch (value) {
            case "A-Z":
                this.filterId = 1;
                break;
            case "Z-A":
                this.filterId = 2;
                break;
            case "Datum Nieuw-Oud":
                this.filterId = 3;
                break;
            case "Datum Oud-Nieuw":
                this.filterId = 4;
                break;
        }

        //append to the choosed filters section
        $('.choosed-filters').empty().append(`<div filter-id="${this.filterId}" class="col-auto option-box align-items-center mr-2 mb-2">
            <div style="vertical-align: middle;text-align: center;">
                <span class="pr-1">${value}</span> 
            </div>
        </div>`).hide().fadeIn();

    }
    /**
     * @description deletes the selected filters.
     */
    handleDeleteFilters() {
        $('.choosed-filters').fadeOut(() => {
            $('.choosed-filters').empty();
            $('.checkbox').prop("checked", false);
        });
    }

    /**
     * @description This function is responsible for sorting events.
     *
     * @returns {Promise<void>}
     */
    async sortEvents() {
        try {
            $("#loader").css('display', 'flex');
            $('#noResult').hide();

            //await keyword 'stops' code until data is returned - can only be used in async function
            const eventsData = await this.eventRepository.getAllEvents();

            //gets all checkboxes
            const elements = document.getElementsByClassName('checkbox');

            //puts all checked checkboxes in an array
            const checkedLocations = Array.prototype.filter.call(elements, (e) => {
                return e.checked
            });

            let filterData = Object.values(eventsData);

            switch (this.filterId) {
                case 1:
                    //sorts by alphabetic order if filter id is 1
                    filterData = filterData.sort((a, b) => {
                        if (a.title < b.title) {
                            return -1;
                        }
                        if (a.title > b.title) {
                            return 1;
                        }
                        return 0;
                    });
                    break;
                case 2:
                    //sorts by descending alphabetic order if filter id is 2
                    filterData = filterData.sort((a, b) => {
                        if (a.title > b.title) {
                            return -1;
                        }
                        if (a.title < b.title) {
                            return 1;
                        }
                        return 0;
                    });
                    break;
                case 3:
                    //sorts by from new to old order if filter id is 3
                    filterData = filterData.sort((a, b) => {
                        return new Date(b.date) - new Date(a.date);
                    });
                    break;
                case 4:
                    //sorts by from old to new order if filter id is 4
                    filterData = filterData.sort((a, b) => {
                        return new Date(a.date) - new Date(b.date);
                    });
                    break;
            }

            //if there are locations checked then the list is going to be filtered
            if (checkedLocations.length !== 0) {
                filterData = filterData.filter(e => {
                    for (let i = 0; i < checkedLocations.length; i++) {
                        if (e.location.includes(checkedLocations[i].value)) {
                            return true;
                        }
                    }
                    return false;
                });
            }

            //empty the existing eventlist
            $('#event-list').empty();

            //appends the new sorted and filtered list
            if (filterData.length === 0) {
                $('#noResult').show();
            } else {
                let eventTitle = (Object.keys(filterData).length === 1 ? " Evenement" : " Evenementen");

                $('span.title').text(eventTitle);
                $('span.events-total').text(Object.keys(filterData).length);
                filterData.forEach(event => {
                    this.createEventLayout(event);
                });
            }

            setTimeout(() => {
                $("#loader").css('display', 'none');
            }, 900);


            $("a.event-button").on("click", (event) => this.handleCountEvent(event));

            this.filterData = filterData;
        } catch (e) {
            console.log("error while fetching events", e);

            $('#noResult').show();
        }
    }
}