/**
 * Responsible for handling the actions happening on welcome view
 *
 * @author Noman Jabbar
 */
class RegisterController extends Controller {

    constructor() {
        super();

        this.userRepository = new UserRepository();

        $.get("views/register.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }

    //Called when the login.html has been loaded
    setup(data) {
        //Load the login-content into memory
        this.registerView = $(data);
        app.loadController(CONTROLLER_SIDEBAR);
        this.registerView.find(".register-form").on("submit", 
        (event) => this.handleRegister(event));

        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.registerView);
    }

    /**
     * @description this allows the users to register.
     * @param event 
     */
    async handleRegister(event){
        event.preventDefault();

        // get variables from thje view
        let email = this.registerView.find("[name='email']").val();
        let password = this.registerView.find("[name='password']").val();
        let passwordConfirm = this.registerView.find("[name='passwordConfirm']").val();
        let status = 0;
        try{
            //checks if the passwords are not the same.
            if(password != passwordConfirm){
                status = 1;
                this.registerView.find(".error").html('<p>De wachtwoorden komen niet overeen.</p>');  
            }
            // checks if the email is empty.
            if(email == ''){
                status = 1;
                this.registerView.find(".error").html('<p>Het veld voor de emailadres mag niet leeg gelaten worden.</p>');
            }
            // checks if the given email is not formatted like an actual email.
            if(this.validateEmail(email) == false){
                status = 1;
                this.registerView.find(".error").html('<p>Dit is geen emailadres.</p>');
            }
            // checks if password is empty.
            if(password == ''){
                status = 1;
                this.registerView.find(".error").html('<p>Het veld voor de wachtwoord mag niet leeg gelaten worden.</p>')
            }
            // checks if the length of the password is less than eight characters.
            if(password.length < 8){
                status = 1;
                this.registerView.find(".error").html('<p>Het wachtwoord mag niet kleiner dan 8 karakters zijn.</p>')

            }
            // if none of the if statements were fired, then everything should be OK.
            if(status == 0){
                // register the user.
                const registerUser = await this.userRepository.register(email, password, passwordConfirm);
                // if it succeeds, login the user immediately.
                if(registerUser.status == 200){
                    
                    const user = await this.userRepository.login(email, password);
                    sessionManager.set("email", user.email);
                    app.loadController(CONTROLLER_ACCOUNT);
                }
            }
        } catch(e) {
            //if this email is already used, show error to frontend.
            if(e.reason.code == 'ER_DUP_ENTRY'){
                this.registerView.find(".error").html("<p>Dit emailadres is al in gebruik!</p>");
            }
            
        }
    }
}