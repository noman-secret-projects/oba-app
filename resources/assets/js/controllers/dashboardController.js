/**
 * @description Controller responsible for showing statistics and recent events with handlers.
 *
 * @author Noman Jabbar
 */
class DashboardController extends Controller {
    constructor() {
        super();
        this.monthArray = ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"];

        this.eventsRepository = new EventRepository()
        $.get("views/dashboard.html")
            .done((htmlData) => this.setup(htmlData))
            .fail((error) => this.error(error));
    }

    /**
     * Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {

        this.dashboardView = $(data);

        this.getData();
        //appends the view in the accountbox
        $(".content").empty().append(this.dashboardView);
    }

    /**
     * @description get the neccessary data from respository.
     */
    async getData() {
        let now = new Date();
        let totalEvents = await this.eventsRepository.getTotalEvents();
        let totalParticipants = await this.eventsRepository.getTotalParticipants();
        // why must you add 1? that is because months range from 0 till 11 so 0 = 1 and so on. 
        // will be in the following format: yyyy-mm-dd.
        let date = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`; 
        let currentEvent = await this.eventsRepository.getTodaysEvent(date);
        let recentEvents = await this.eventsRepository.getRecentEvents();
        recentEvents = recentEvents.output;
        
        this.dashboardView.find(".timer.events").attr("data-to", totalEvents.output);
        this.dashboardView.find(".timer.participants").attr("data-to", totalParticipants.output);
        this.dashboardView.find(".bg-success > h5").text(currentEvent.title);
        
        recentEvents.forEach(event => {
            this.creatTableLayout(event);
        });

        $("a.table-button").on("click", (event) => this.handleCountEvent(event));

        this.numberAnimation();
    }
    /**
     * @description creates the table row layout for each event.
     * @param {Object} data 
     */
    creatTableLayout(data) {
        let date = new Date(data.date);
        $("table.table-bordered > tbody").append(`
        <tr>
            <td>${date.getDate()} ${this.monthArray[date.getMonth()]} ${date.getFullYear()}</td>
            <th>${data.title}</th>
            <td>${data.time.slice(0, -3)} uur</td>
            <td><b>${data.amount}</b> deelnemers</td>
            <td class="button-wrapper" data-event-id="${data.id}">
                <a id="event_button" class="table-button" data-controller="welcome">Tellen</a>
            </td>
        </tr>
        `);
    }
    /**
     * @description this function handles the animation for the given numbers.
     */
    numberAnimation() {
        (function ($) {
            $.fn.countTo = function (options) {
                options = options || {};

                return $(this).each(function () {
                    // set options for current element
                    var settings = $.extend({}, $.fn.countTo.defaults, {
                        from: $(this).data('from'),
                        to: $(this).data('to'),
                        speed: $(this).data('speed'),
                        refreshInterval: $(this).data('refresh-interval'),
                        decimals: $(this).data('decimals')
                    }, options);

                    // how many times to update the value, and how much to increment the value on each update
                    var loops = Math.ceil(settings.speed / settings.refreshInterval),
                        increment = (settings.to - settings.from) / loops;

                    // references & variables that will change with each update
                    var self = this,
                        $self = $(this),
                        loopCount = 0,
                        value = settings.from,
                        data = $self.data('countTo') || {};

                    $self.data('countTo', data);

                    // if an existing interval can be found, clear it first
                    if (data.interval) {
                        clearInterval(data.interval);
                    }
                    data.interval = setInterval(updateTimer, settings.refreshInterval);

                    // initialize the element with the starting value
                    render(value);

                    function updateTimer() {
                        value += increment;
                        loopCount++;

                        render(value);

                        if (typeof (settings.onUpdate) == 'function') {
                            settings.onUpdate.call(self, value);
                        }

                        if (loopCount >= loops) {
                            // remove the interval
                            $self.removeData('countTo');
                            clearInterval(data.interval);
                            value = settings.to;

                            if (typeof (settings.onComplete) == 'function') {
                                settings.onComplete.call(self, value);
                            }
                        }
                    }

                    function render(value) {
                        var formattedValue = settings.formatter.call(self, value, settings);
                        $self.html(formattedValue);
                    }
                });
            };

            $.fn.countTo.defaults = {
                from: 0, // the number the element should start at
                to: 0, // the number the element should end at
                speed: 1000, // how long it should take to count between the target numbers
                refreshInterval: 100, // how often the element should be updated
                decimals: 0, // the number of decimal places to show
                formatter: formatter, // handler for formatting the value before rendering
                onUpdate: null, // callback method for every time the element is updated
                onComplete: null // callback method for when the element finishes updating
            };

            function formatter(value, settings) {
                return value.toFixed(settings.decimals);
            }
        }(jQuery));

        jQuery(function ($) {
            // custom formatting example
            $('.count-number').data('countToOptions', {
                formatter: function (value, options) {
                    return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
                }
            });

            // start all the timers
            $('.timer').each(count);

            function count(options) {
                var $this = $(this);
                options = $.extend({}, options || {}, $this.data('countToOptions') || {});
                $this.countTo(options);
            }
        });
    }
}