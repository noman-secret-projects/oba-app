/**
 * Responsible for handling the actions happening on sidebar view
 *
 * @author Noman Jabbar
 */
class NavbarController extends Controller {
    constructor() {
        super();

        $.get("views/navbar.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }

    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        //Load the sidebar-content into memory
        const sidebarView = $(data);
        //Find all anchors and register the click-event
        sidebarView.find("a").on("click", (event) => this.handleClickBtn(event));
        
        // check if hamburger is not hidden.
        sidebarView.find('#nav-icon').on('click', () => {
            $('div.sidebar').toggleClass('open');
        });

        this.hideItems(sidebarView);
        this.setItemActive(sidebarView);
        //Empty the sidebar-div and add the resulting view to the page
        $(".sidebar").empty().append(sidebarView);

        return sidebarView;
    }

    /**
     * @description when logged in, certain items need to be hidden like the login and register.
     * @param {JQuery} view
     */
    hideItems(view){

        if(!sessionManager.get("email")) {
            view.find("a[data-controller='logout']").parent().remove();
            view.find("a[data-controller='account']").parent().remove();
            view.find("a.nav-link[data-controller='welcome']").parent().remove();
            view.find("a[data-controller='events']").parent().remove();
            view.find("a[data-controller='location']").parent().remove();
            view.find("a[data-controller='dashboard']").parent().remove();
        }else{
            view.find("a[data-controller='register']").parent().remove();
            view.find("a[data-controller='login']").parent().remove();
        }
    }

    /**
     * @description gets the view and sets the active item based on hash in the url.
     * @param {Jquery} view 
     */
    setItemActive(view){
        view.find('ul.navbar-nav').children().each((index, element) => {
            // checks if the current hash is the same as the data-controller link.
            if(location.hash.substr(1) == $(element).find('a').attr('data-controller')){
                $(element).find('a').toggleClass('active');
            }else{
                $(element).find('a').removeClass('active');
            }
        });
    }
}
