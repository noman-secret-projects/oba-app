/**
 * @description Controller responsible for linking NFC tags. Sets a cookie so the nfcCountedController can link the tag.
 *
 * @author Sam Overheul
 */
class NfcController extends Controller {

    constructor() {
        super();

        this.eventRepository = new EventRepository();

        $.get("views/nfc.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }
    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        this.nfc = $(data);
        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.nfc);

        if (!isNaN(this.getId())) {
            this.createCookie("event_to_nfc", this.getId(), 5)
        } else {
            app.loadController("events");
        }
    }
    /**
     * @description get the id out of the url.
     * @returns {Number}
     */
    getId() {
        let params = new URLSearchParams(location.search);
        return parseInt(escape(params.get("id")));
    }
}