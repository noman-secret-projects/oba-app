/**
 * @description Controller responsible for all events in view and setting data
 *
 * @author Adam Abbas & Justus Ventross
 */
class AccountController extends Controller{

    constructor() {
        super();
        this.userRepository = new UserRepository();

        $.get("views/account.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }

    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        //Load the login-content into memory
        this.accountView = $(data);
        this.accountView.find("#editbtn").click((event) => this.handleClickBtn(event) );

        //reloads the topbar
        app.loadController(CONTROLLER_SIDEBAR);

        this.accountView.ready(() => this.getUser());
        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.accountView);
    }

    /**
     * @returns Gets email from db and appends it into the relevant div's
     *
     * @returns {Promise<void>}
     */
    async getUser() {
        let user = await this.userRepository.getUser(sessionManager.get("email"));

        $('#name').append(user.email);
    }
}