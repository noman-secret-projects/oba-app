/**
 * @description Controller responsible for all events in view and setting data
 *
 * @author Adam Abbas & Justus Ventross
 */
class EditAccountController extends Controller {
    constructor() {
        super();

        this.userRepository = new UserRepository();

        $.get("views/edit_account.html")
            .done((htmlData) => this.setup(htmlData))
            .fail((error) => this.error(error));
    }

    /**
     * Loads up page into the container
     *
     * @param htmlData html page that's going to be loaded
     */
    setup(htmlData) {
        this.editAccountView = $(htmlData);
        app.loadController(CONTROLLER_SIDEBAR);

        this.editAccountView.ready(() => {
            this.editAccountView.find("#cancelbtn").on("click", (event) => this.handleClickBtn(event));
            this.editAccountView.find("#confirmbtn").on("click", () => this.handleConfirm());
        });

        //appends the view in the accountbox
        $(".content").empty().append(this.editAccountView);

        //sets email in the inputfield
        $("#email").val(sessionManager.get("email"));
    }

    /**
     * @description Handles confirm button
     *
     * @returns {Promise<void>}
     */
    async handleConfirm() {
        const NEW_email = $("#email").val();
        const OLD_PASSWORD = $('#old-password').val();
        const NEW_PASSWORD = $('#new-password').val();
        const CONFIRM_NEW_PASSWORD = $('#confirm-new-password').val();
        const BAD_REQUESTCODE = 400;
        const UNAUTHORIZED_CODE = 401;

        try {

            //rows of if-statements that check whether the input values are valid
            if (!this.validateEmail(NEW_email)) {
                this.setErrorMessage('Voer een correct e-mailadres in!');
            } else if (this.checkPassword(NEW_PASSWORD, CONFIRM_NEW_PASSWORD)) {

                //skips doing requests to the database if the old-password field is empty
                if (OLD_PASSWORD !== "") {

                    let oldPassword = await this.userRepository.login(sessionManager.get("email"), OLD_PASSWORD);
                    let updatedPassword = await this.userRepository.updatePassword(sessionManager.get("email"), NEW_PASSWORD);
                }

                //doing requests to the database
                let updatedMail = await this.userRepository.updateUser(NEW_email, sessionManager.get("email"));

                //if the returned code is 400, the email is already in use
                if (updatedMail.code === BAD_REQUESTCODE) {
                    this.setErrorMessage('Het e-mail is al in gebruik!');
                } else {
                    //brings out loader
                    $("#loader").css('display', 'flex');

                    sessionManager.set("email", NEW_email);

                    app.loadController("account");
                }

                //loader goes away after timestamp
                setTimeout(() => {
                    $("#loader").css('display', 'none');
                }, 900);
            }
        } catch (e) {

            //if the catched exception code is 401, the password is wrong
            if (e.code === UNAUTHORIZED_CODE) {
                this.setErrorMessage('Oud wachtwoord is onjuist!');
            } else {
                this.setErrorMessage('Er is iets misgegaan, probeer het later opnieuw!')
            }
        }
    }

    /**
     * @description Checks if password is valid. The password will also be valid if it's empty, because changing a password is not a must.
     *
     * @param {String} password the password that's going to be checked
     * @param {String} confirmPassword the password that's put into the 'confirm password' field
     * @returns {Boolean} returns true if password is valid or empty
     */
    checkPassword(password, confirmPassword) {
        const MAX_CHARACTERS = 8;
        let boolean = true;

        if (password !== "") {
            if (password.length < MAX_CHARACTERS) {
                this.setErrorMessage('Wachtwoord moet minimaal 8 karakters zijn!');
                boolean = false;
            } else if (password !== confirmPassword) {
                this.setErrorMessage('Het bevestigde wachtwoord komt niet overeen!');
                boolean = false;
            }
        }
        return boolean;
    }
}