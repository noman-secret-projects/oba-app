/**
 * @description Controller responsible for showing events attached to locations.
 *
 * @author Sahil Mankani
 */
class LocationController extends Controller {

    constructor() {
        super();

        this.eventRepository = new EventRepository();

        $.get("views/location.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }
    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        this.locationView = $(data);
        $(".content").empty().append(this.locationView);
        this.showPosition();

        app.loadController(CONTROLLER_SIDEBAR);

        $('#map').on('click', '#btn-loc-1',  () => {
            this.clearResults();
            this.sortLocation('Oosterdokskade 143');
        });

        $('#map').on('click', '#btn-loc-2',  () => {
            this.clearResults();
            this.sortLocation('Mauritskade 58');
        });

        $('#map').on('click', '#btn-loc-3',  () => {
            this.clearResults();
            this.sortLocation('Plantage Middenlaan 2a');
        });
    };

    /**
     * @description this function displays map and locations.
     *
     */
    showPosition() {
        // TO MAKE THE MAP APPEAR YOU MUST
        // ADD YOUR ACCESS TOKEN FROM
        // https://account.mapbox.com
        mapboxgl.accessToken = 'pk.eyJ1Ijoic2FoaWxvZmZpY2lhbCIsImEiOiJjazlqNmkwc2QwY2trM2huenAzdTk0Z2s2In0.uUzMe_iLfH2wdGAi-ZRPBA';
        var map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [ 4.8945, 52.3667], // starting position
            zoom: 12 // starting zoom
        });


        var popup1 = new mapboxgl.Popup({ offset: 25 })
            .setHTML('<strong>Oosterdokskade 143</strong><p>Klik hier voor alle evenementen op deze locatie.</p><button id="btn-loc-1" type="button" class="btn btn-outline-danger">Evenementen</button>')
            .addTo(map)

        var popup2 = new mapboxgl.Popup({ offset: 25 })
            .setHTML('<strong>Mauritskade 58</strong><p>Klik hier voor alle evenementen op deze locatie.</p><button id="btn-loc-2" type="button" class="btn btn-outline-danger">Evenementen</button>')
            .addTo(map)

        var popup3 = new mapboxgl.Popup({ offset: 25 })
            .setHTML('<strong>Plantage Middenlaan 2a</strong><p>Klik hier voor alle evenementen op deze locatie.</p><button id="btn-loc-3" type="button" class="btn btn-outline-danger">Evenementen</button>')
            .addTo(map)

        var marker1Cord=[4.9082058915060145, 52.376075927219574];
        var marker2Cord=[4.918498161487435, 52.36188644362619];
        var marker3Cord=[4.9075985034273515,52.36717224136393];

        var marker1 = new mapboxgl.Marker()
            .setLngLat(marker1Cord)
            .setPopup(popup1) // sets a popup on this marker
            .addTo(map);


        var marker2 = new mapboxgl.Marker()
            .setLngLat(marker2Cord)
            .setPopup(popup2) // sets a popup on this marker
            .addTo(map);


        var marker3 = new mapboxgl.Marker()
            .setLngLat(marker3Cord)
            .setPopup(popup3) // sets a popup on this marker
            .addTo(map);

        map.addControl(
            new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            })
        );
    };
    /**
     * @description creates the layout of the results.
     * @param {Object} data 
     */
    createEventLayout(data) {
        $('#event-list').append(
            `<div id='eventlayout' class="event col-lg-6 col-sm-12" data-event-id="${data.id}">
            <h4 class="event-title">${data.title}</h4>
            <p class="event-details-btn" data-toggle="collapse" href="#eventDetails${data.id}" role="button" aria-expanded="false" aria-controls="eventDetails">Meer info</p>
            <ul id="eventDetails${data.id}" class="list-group list-group-flush collapse">
                <li class="list-group-item">${data.subject}</li>
                <li class="list-group-item">${data.info}</li>
                <li class="list-group-item">${data.location}</li>
                <li class="list-group-item">${data.tutor}</li>
                <li class="list-group-item">&euro; ${data.price}</li>
                <li class="list-group-item">Aantal personen: <span class="badge badge-info badge-pill">${data.amount}</span></li>
            </ul>
            <div class="event-button-wrapper">
                <a id="event_button" class="btn event-button" data-controller="welcome">Tellen</a>
            </div>
	    </div>`);
    }
    /**
     * @description get the events of the location.
     * @param {String} location 
     */
    async sortLocation(location){
        try {
            //await keyword 'stops' code until data is returned - can only be used in async function

            const eventsData = await this.eventRepository.getAllEvents();

            let filterData = Object.values(eventsData);

                filterData = filterData.filter(e => {
                        if (e.location.includes(location)) {
                            this.createEventLayout(e);
                        }
                });

            $("a.event-button").on("click", (event) => this.handleCountEvent(event));

        }catch (e) {
            console.log("error while fetching events", e);

            $('#noResult').show();
        }

    }
    /**
     * @description this function clears the results list.
     */
    clearResults(){
        document.querySelector('.location-events-list').innerHTML='';
    }
}