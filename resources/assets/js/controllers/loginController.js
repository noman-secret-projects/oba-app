/**
 * @description Controller responsible for all events in view and setting data
 *
 * @author Ryan Veerman
 */
class LoginController extends Controller {

    constructor() {
        super();

        this.userRepository = new UserRepository();

        $.get("views/login.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }

    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        //reload the topbar
        app.loadController("sidebar");
        //Load the login-content into memory
        this.loginView = $(data);
        this.loginView.find(".login-form").on("submit", (e) => this.handleLogin(e));

        this.loginView.find(".btn-2").on("click", () => {
            $("#passModal").modal('show');
        });

        this.loginView.find(".modal-header .close").on("click", () => {
            $("#passModal").modal('hide');
        });

        this.loginView.find("#sendMail").on("click", () => this.forgotPassword());

        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.loginView);
    }

    /**
     * @description Async function that does a login request via repository
     * @param {Event} event
     */
    async handleLogin(event) {
        //prevent actual submit and page refresh
        event.preventDefault();

        //Find the email and password
        const email = this.loginView.find("[name='email']").val();
        const password = this.loginView.find("[name='password']").val();

        try {
            //await keyword 'stops' code until data is returned - can only be used in async function
            const user = await this.userRepository.login(email, password);

            sessionManager.set("email", user.email);
            app.loadController(CONTROLLER_EVENTS);
        } catch (e) {
            this.loginView.find(".login-invoerveld").addClass("incorrect-information");

            //if unauthorized error show error to user
            if (e.code === 401) {
                this.loginView
                    .find(".error")
                    .html(e.reason);
            } else {
                console.log(e);
            }
        }
    }

    /**
     * @description when clicked on the send email button in the modal.
     */
    async forgotPassword() {

        let email = $(".modal-email").val();

        if (email.trim() === "") {
            this.loginView.find(".modal-error").empty().html('Voer uw mail in!');
            this.loginView.find(".modal-email").addClass("incorrect-information");
        }

        if (this.validateEmail(email)) {
            try {
                let checkmail = await this.userRepository.getUser(email);

                let mail = await this.userRepository.forgotPassword(email);
                this.loginView.find(".modal-email").removeClass("incorrect-information");
                this.loginView.find(".modal-error").removeClass("text-danger").addClass("text-success").empty().text("De mail is verstuurd!");

                if (mail.mail) {
                    // dan pas sluiten.
                    setTimeout(() => {
                        $("#passModal").modal('hide');
                    }, 2000);
                }
            } catch (e) {
                this.loginView.find(".modal-email").addClass("incorrect-information");
                this.loginView.find(".modal-error").empty().html('Deze email is nog niet geregistreerd');
            }
        } else {
            this.loginView.find(".modal-email").addClass("incorrect-information");
        }
    }
}