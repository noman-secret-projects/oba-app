/**
 * Responsible for handling the actions happening on welcome view
 *
 * @author Noman Jabbar
 */
class WelcomeController extends Controller {
    constructor() {
        super();

        $.get("views/welcome.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }

    setup(data) {
        app.loadController(CONTROLLER_SIDEBAR);
        //Load the login-content into memory
        this.welcomeView = $(data);
        // Add handclickbtn handler to all the buttons on view.
        this.welcomeView.find("button").on("click", (event) => this.handleClickBtn(event));
        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.welcomeView);
    }
}