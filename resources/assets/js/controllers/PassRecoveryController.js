/**
 * @description Controller responsible for checking email and letting you pick a new password.
 *
 * @author Noman Jabbar, Sahil Mankani & Ryan Veerman.
 */
class PassRecoveryController extends Controller{

    constructor() {
        super();
        this.userRepository = new UserRepository();

        $.get("views/pass_recovery.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }

    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        //Load the login-content into memory
        this.recoveryView = $(data);
        //reloads the topbar
        app.loadController(CONTROLLER_SIDEBAR);
        // get url parameters through the builtin URLSearchParams Object.
        // @todo: check this part, there might be a bug innit. ~Normie
        this.urlParams = new URLSearchParams(location.search);

        this.recoveryView.ready(() =>{
            $("#loader").css('display', 'flex');
            this.validateEmail();
        });
        this.recoveryView.find('#confirmbtn').click(() => this.handleConfirm());
        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.recoveryView);
    }

    /**
     * @description Gets email and validates it.
     * @returns {Promise<void>}
     */
    async validateEmail() {

        let verifiedEmail = await this.userRepository.verifyEmail(this.urlParams.get("uid"), this.urlParams.get('hash'));

        this.email = verifiedEmail.email;

        if(verifiedEmail.status == 200) {
            // show edit form after 2 seconds (2000 milliseconds)
            setTimeout(() => { 
                $("#loader").css('display', 'none'); 
                $(".edit-form").css('display', 'block'); 
            }, 1000);
        }
    }
    /**
     * @description Handles confirm button
     * @returns {Promise<void>}
     */
    async handleConfirm() {
        const NEW_PASSWORD = $('#new-password').val();
        const CONFIRM_NEW_PASSWORD = $('#confirm-new-password').val();
        const UNAUTHORIZED_CODE = 401;

        try {

            //rows of if-statements that check whether the input values are valid
            if (this.checkPassword(NEW_PASSWORD, CONFIRM_NEW_PASSWORD)) {

                let updatedPassword = await this.userRepository.updatePassword(this.email, NEW_PASSWORD);

                sessionManager.set("email", this.email);

                app.loadController("account");
            }
        } catch (e) {

            //if the catched exception code is 401, the password is wrong
            if (e.code === UNAUTHORIZED_CODE) {
                this.setErrorMessage('Oud wachtwoord is onjuist!');
            } else {
                console.log(e.message);
                this.setErrorMessage('Er is iets misgegaan, probeer het later opnieuw!')
            }
        }
    }
}