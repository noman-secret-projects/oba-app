/**
 * @description Controller responsible for checking NFC tags. Can link a NFC tag or can count a visitor of the page.
 *
 * @author Sam Overheul
 */
class NfcCountedController extends Controller {

    constructor() {
        super();

        this.roomRepository = new RoomRepository();

        $.get("views/nfc_counted.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }
    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        let params = new URLSearchParams(location.search);
        let roomId = parseInt(escape(params.get("loc")));

        if (this.getCookie("event_to_nfc") == null) {
            this.nfcCounted = $(data);
            //Empty the content-div and add the resulting view to the page
            $(".content").empty().append(this.nfcCounted);

            if (this.getCookie("isCounted") !== "true") {
                this.count(roomId);
                this.isCounted();
            }

        } else {
            this.setTagToRoom(roomId);

            $.get("views/nfc_linked.html")
                .done((data) => this.loadEventTitle(data))
                .fail((error) => this.error(error));
        }

        this.loadRoomSpecificInfo(roomId);
    }
    /**
     * @description A function that fires if an event is counted.
     *
     */
    isCounted() {
        this.createCookie("isCounted", "true", 45);
    }

    /**
     * @description Loads up the details of a room.
     *
     * @param {Number} roomId
     */
    async loadRoomSpecificInfo(roomId) {

        switch (roomId) {
            case 1:
                $.get("views/rooms/oba_forum.html")
                    .done((data) => $(".room-information").empty().append(data))
                    .fail((error) => this.error(error));
                break;

            case 2:
                $.get("views/rooms/oba_theater.html")
                    .done((data) => $(".room-information").empty().append(data))
                    .fail((error) => this.error(error));
                break;

            case 3:
                $.get("views/rooms/oba_vergaderzaal_6_1.html")
                    .done((data) => $(".room-information").empty().append(data))
                    .fail((error) => this.error(error));
                break;

            default:
                $.get("views/rooms/oba_default.html")
                    .done((data) => $(".room-information").empty().append(data))
                    .fail((error) => this.error(error));
                break;
        }

    }
    /**
     * @description Loads up title of an event.
     *
     * @param data html page that's going to be loaded
     */
    async loadEventTitle(data) {
        this.nfcLinked = $(data);
        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.nfcLinked);

        var now = new Date();
        now.setMinutes(now.getMinutes() + 45); // timestamp
        now = new Date(now);

        let hour = ("0" + now.getHours()).slice(-2);
        let minute = ("0" + now.getMinutes()).slice(-2);

        $(".page-title").append(hour +":" + minute);

    }
    /**
     * @description add a count to the room.
     *
     * @param {Number} roomId
     */
    async count(roomId) {
        this.getEventId(roomId).then((data) => {
            this.roomRepository.addCount(data.eventId);
        });
    }
    /**
     * @description get the event id which is attached to the room.
     *
     * @param {Number} roomId
     */
    async getEventId(roomId) {
        let time = new Date();
        let timestamp = Math.round(time.getTime() / 1000);

        return await this.roomRepository.getEvent(roomId, timestamp);
    }
    /**
     * @description get a cookie by name.
     *
     * @param {String} name
     */
    getCookie(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    }
    /**
     * @description set the tag for a room.
     *
     * @param {Number} roomId
     */
    setTagToRoom(roomId) {
        let time = new Date();
        let timestamp = Math.round(time.getTime() / 1000) + 45 * 60;
        let eventId = parseInt(this.getCookie("event_to_nfc"));

        this.roomRepository.setNfcTag(roomId, timestamp, eventId);
    }
}
