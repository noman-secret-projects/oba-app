/**
 * @description This controller is the parent controller, it's responsible for functions that are used by two or more controllers.
 * 
 * @author Noman Jabbar
 */
class Controller {
    /**
     * constructor - instantiate queryparams if needed.
     * @param boolean queryParams 
     */
    constructor(queryParams) {
        this.queryParams = queryParams;
    }
    /**
     * @description checks if there is an id in the url. 
     * @returns {Boolean}
     */
    checkqueryParams() {
        let urlParams = new URLSearchParams(location.search.slice(0, -1));

        if (this.queryParams == true && urlParams.has("id")) {

            return true;
        }
        return false;
    }
    /**
     * show page error if needed.
     * @param {*} error 
     */
    error(error) {
        $(".content").html(`${error.responseText}\n So that went wrong 😒`);
    }
    /**
     * @description handles the click event on the given element.
     * @param {Event} event 
     * @return {Boolean}
     */
    handleClickBtn(event) {
        let controller;
        // check if the current event.target (the element who fired this event) has an id, if it does it probably is not an element which needs to hide the sidebar.
        if (!event.target.hasAttribute("id")) $('.sidebar').toggleClass('open');
        //Get the data-controller from the clicked element (this)
        if ($(event.target).hasClass("logo")) {
            // let controller = $(event.target).attr("data-controller");
            controller = $(event.target.parentElement).attr("data-controller");
        } else {
            controller = $(event.target).attr("data-controller");
        }

        //Pass the action to a new function for further processing
        app.loadController(controller);

        //Return false to prevent reloading the page
        return false;
    }
    /**
     * @description handles the click event specially for the count buttons.
     * @param {Event} event 
     */
    handleCountEvent(event) {
        let id = $(event.target.offsetParent).attr("data-event-id");
        // escape special characters because they are NOT needed here/
        app.loadController(CONTROLLER_WELCOME, {id: escape(id)});
    }
    /**
     * @description check email if it's valid through regular expression.
     * @param {String} email
     * @returns {Boolean} returns true if email is valid else it's false.
     */
    validateEmail(email) {
        // const RE = /\w*[a-zA-Z]\w*/; this regex was used for the name, keep this.
        const RE = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i;
        return RE.test(email);
    }
    /**
     * Checks if password is valid. The password will also be valid if it's empty, because changing a password is not a must.
     *
     * @param {String} password the password that's going to be checked
     * @param {String} confirmPassword the password that's put into the 'confirm password' field
     * @returns {Boolean} returns true if password is valid or empty
     */
    checkPassword(password, confirmPassword) {
        const MAX_CHARACTERS = 8;
        let boolean = true;

        if (password !== "") {
            if (password.length < MAX_CHARACTERS) {
                this.setErrorMessage('Wachtwoord moet minimaal 8 karakters zijn!');
                boolean = false;
            } else if (password !== confirmPassword) {
                this.setErrorMessage('Het nieuwe en oude wachtwoord komen niet overeen!');
                boolean = false;
            }
        }
        return boolean;
    }
    /**
     * @description Sets content of errormessage
     *
     * @param {String} errorMessage the content
     */
    setErrorMessage(errorMessage) {
        $('#error').text(errorMessage).show();
    }
    /**
     * @description Checks whether a string contains only digits
     *
     * @param {String} string
     * @returns {boolean}
     */
    containDigits(string) {
        const RE = /^[0-9]*$/;
        return RE.test(string);
    }
    /**
     * @description creates a cookie.
     *
     * @param {String} name
     * @param {Any} value
     * @param {Number} minutes
     */
    createCookie(name, value, minutes) {
        let expires;

        if (minutes) {
            let date = new Date();
            date.setTime(date.getTime() + (minutes * 60 * 1000) + (date.getTimezoneOffset() * 60 *1000));
            expires = "; expires=" + date;
        } else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }
}