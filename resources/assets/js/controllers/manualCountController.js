/**
 * @description Controller responsible for all events in view and setting data
 *
 * @author Adam Abbas & Noman Jabbar.
 */
class ManualCountController extends Controller {
    constructor() {
        super(true);
        this.eventRepository = new EventRepository();

        $.get("views/manual_count.html")
            .done((data) => this.setup(data))
            .fail((error) => this.error(error));
    }
    /**
     * @description Loads up page into the container
     *
     * @param data html page that's going to be loaded
     */
    setup(data) {
        app.loadController(CONTROLLER_SIDEBAR);

        //Load the content into memory
        this.buttonView = $(data);

        if (this.checkqueryParams()) {
            this.handleSearch(this.getId());
        } else {
            app.loadController("events");
        }
        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(this.buttonView);

        this.buttonView.find(".count").on("click", (event) => this.handleCountCheck($(event.target)));
        this.buttonView.find("#confirmInputBtn").on("click", () => this.handleCountCheck($("#manualInput")));

        this.buttonView.find("#subtract").on("click", (event) => {

            if (document.getElementById('subtract').checked) {
                $(".count").toggleClass("disabled");
                this.buttonView.find(".count").off("click");
            } else {
                $(".count").toggleClass("disabled");
                this.buttonView.find(".count").on("click", (event) => this.handleCountCheck($(event.target)));
            }
        });
    }
    /**
     * @description get the id out of the url.
     * @returns {Number}
     */
    getId() {
        // get the queryparameters trough the URLSearchParams Interface, 0 stands for the position so the begin and -1 is to remove the last character from the query.
        let queryParams = new URLSearchParams(location.search.slice(0, -1));
        let id = parseInt(queryParams.get("id"));

        return id;
    }

    /**
     * @description This function gets all info of an event from db.
     *
     * @param eventId the event id of the event
     * @returns {Promise<void>}
     */
    async handleSearch(eventId) {

        try {
            this.event = await this.eventRepository.getEvent(eventId);

            $('#eventName').empty().append(this.event.name);
            $('#amount').empty().append(this.event.amount);
            this.eventId = parseInt(eventId);


        } catch (e) {
            $('#amount').empty();
            $('#eventName').empty().append(`Evenement niet gevonden!`).css("color", "red");
        }

    }

    /**
     * @description Checks amount if it's a valid number.
     * @param {JQuery} element
     * @returns {Promise<void>}
     */
    async handleCountCheck(element) {
        const CHECKBOX = document.getElementById("subtract");
        const MAX_WARNING_VALUE = 1;

        $("#error").empty();

        //checks if element value are digits.
        if (this.containDigits(element.val()) && element.val() !== "") {
            //if the clicked value is higher then 1, the user gets a confirm warning
            if (element.val() > MAX_WARNING_VALUE) {
                bootbox.confirm({
                    centerVertical: true,
                    title: "Aantal bevestigen",
                    message: `Weet u zeker dat u ${element.val()} personen wilt <b>${(element.hasClass('minus') || CHECKBOX.checked) ?
                        'aftrekken' : 'optellen'}</b>?`,
                    buttons: {
                        confirm: {
                            label: 'Bevestigen',
                            className: 'btn btn-primary btn-oba modal-buttons'
                        },
                        cancel: {
                            label: 'Annuleren',
                            className: 'btn btn-secondary modal-buttons'
                        }
                    },
                    callback: (result) => {
                        if (result) {
                            this.handleCount(element);
                        }
                    }
                });
            } else {
                this.handleCount(element);
            }
        } else {
            $("#error").append(`Uw invoer is geen getal!`);
        }
    }

    /**
     * @description Counts the amount of people at the event. This function gets fired when someone clicks on the count button.
     * @param {JQuery} element
     * @returns {Promise<void>}
     */
    async handleCount(element) {
        const CHECKBOX = document.getElementById("subtract");
        let handleCount;

        $("#loader").css('display', 'flex');
        try {

            //checks what type of count method is used
            switch (true) {
                case element.hasClass('plus'):
                    handleCount = await this.eventRepository.setAmount(this.eventId, this.event.amount + parseInt(element.val()));
                    this.handleSearch(this.eventId);
                    break;
                case element.hasClass('minus'):
                    if (this.event.amount - element.val() >= 0)
                        handleCount = await this.eventRepository.setAmount(this.eventId, this.event.amount - parseInt(element.val()));
                    else $("#error").append(`Aantal wordt hierdoor kleiner dan 0!`);
                    this.handleSearch(this.eventId);
                    break;
                case element.hasClass('manual'):
                    if (!CHECKBOX.checked)
                        handleCount = await this.eventRepository.setAmount(this.eventId, this.event.amount + parseInt(element.val()));
                    else {
                        if (this.event.amount - element.val() >= 0)
                            handleCount = await this.eventRepository.setAmount(this.eventId, this.event.amount - parseInt(element.val()));
                        else $("#error").append(`Aantal wordt hierdoor kleiner dan 0!`);
                    }
                    element.val("");
                    this.handleSearch(this.eventId);
                    break;
                default:
                    $("#error").append(`Er is iets fout gegaan!`);
                    break;
            }

            setTimeout(() => {
                $("#loader").css('display', 'none');
            }, 400);
            $("#manualInput").val("");
        } catch (e) {
            console.log(e);
            $("#error").append(`Er is iets fout gegaan!`);
        }
    }
}