const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const terser = require('gulp-terser');

gulp.task('sass-dev', () => {
    return gulp.src('resources/assets/sass/app.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('resources/dist/css'))
});

gulp.task('js-dev', () => {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/mapbox-gl/dist/mapbox-gl.js',
        'resources/assets/js/utils/*.js',
        'resources/assets/js/controllers/controller.js',
        'resources/assets/js/controllers/*Controller.js',
        'resources/assets/js/repositories/*.js',
        'resources/assets/js/app.js',
        'node_modules/bootbox/dist/bootbox.min.js'
    ])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('resources/dist/js'))
});

gulp.task('sass-prod', () => {
    return gulp.src('resources/assets/sass/app.scss')
    .pipe(sass({
        errLogToConsole: false,
        outputStyle: 'compressed'
    })) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('resources/dist/css'))
});

gulp.task('js-prod', () => {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/mapbox-gl/dist/mapbox-gl.js',
        'resources/assets/js/utils/*.js',
        'resources/assets/js/controllers/controller.js',
        'resources/assets/js/controllers/*Controller.js',
        'resources/assets/js/repositories/*.js',
        'resources/assets/js/app.js',
        'node_modules/bootbox/dist/bootbox.min.js'
    ])
    .pipe(concat('all.js'))
    .pipe(terser({
        output: {
            comments: /^\!/ // keep comments that start with "/*!"
        }
    }))
    .pipe(gulp.dest('resources/dist/js'))
});

gulp.task('clean:public', function() {
    return del.sync('resources/dist/css')
    .pipe(del.sync('resources/dist/js'));
});