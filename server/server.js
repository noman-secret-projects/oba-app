/**
 * Do not change this - "main" entry point of the server
 *
 * Add your server logic to app.js
 *
 * @author Noman Jabbar
 */
const dotenv = require("dotenv");
const SERVER_PORT = process.env.PORT || 3000;
// dotenv.config retrieves all environment variables from the file named ".env".
dotenv.config();
const app = require('./app');
app.listen(SERVER_PORT, () => console.log(`PAD Framework app listening on port ${SERVER_PORT} for environment ${process.env.NODE_ENV}!`));