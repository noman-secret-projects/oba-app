/**
 * Database connection pool with MySQL
 *
 * This class uses config from config/users.json - make sure you fill in the right details there found on PAD cloud!
 *
 * @author Pim Meijer & Lennard Fonteijn
 */
const mysql = require("mysql");
/**
 * Makes a connection to the database. Only do this once in application lifecycle.
 */
function init() {

    if(!process.env.DB_HOST || !process.env.DB_SCHEMA || !process.env.DB_USER || !process.env.DB_PASS) {
        
        return;
    }

    let connectionPool;

    connectionPool = mysql.createPool({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_SCHEMA,
        connectionLimit : process.env.CONN_LIMIT, //NOTE: Each team only has a maximum of 10 connections, this includes MySQL Workbench connections.
        timezone: "UTC",
        multipleStatements: true
    });

    //Quicktest connection for errors
    connectionPool.getConnection((err, conn) => {
        if(err) {
            console.log(err);
            console.log(`${err.errno} ${err.code}: ${err.sqlMessage}`);
        } else {
            conn.release();
        }
    });

    return connectionPool;
}

/**
 * Use this function for all queries to database - see example in app.js
 * @param connectionPool
 * @param data contains query with "?" parameters(values)
 * @param successCallback - function to execute when query succeeds
 * @param errorCallback - function to execute when query fails
 */
function handleQuery(connectionPool, data, successCallback, errorCallback) {
    connectionPool.query({
        sql: data.query,
        values: data.values,
        timeout: process.env.TIMEOUT
    }, (error, results) => {
        if (error) {
            console.log(error);
            errorCallback(error);
        } else {
            successCallback(results);
        }
    });
}

module.exports = {
    init,
    handleQuery
};
