// instead of using the http(s) module from nodejs, I went with axios because I'm familiar with it and it's an easy to use http client.
const axios = require("axios").default;
const convertor = require("xml-js");
/**
 * OBA API Wrapper - contains the used api endpoint from OBA.
 * 
 * @author Noman Jabbar
 */
class OBA {
    constructor () {
        // variables for building up the url.
        this.baseUrl = "https://zoeken.oba.nl/api/v1";
        this.endpoint = "/search";
        // parameters when doing a request.
        this.query = "table:jsonsrc";
        this.refine = false;
        this.response = undefined;
        // public key, also used in the parameters.
        this.pub = process.env.OBA_PUBLIC;
        // secret ONLY goes in the headers.
        this.secret = process.env.OBA_SECRET;

        this.axiosInstance = axios.create({
            baseURL: this.baseUrl,
            timeout: 3000,
            headers: {
                "AquaBrowser": this.secret,
                // keep this on xml because their json output is still in beta.
                "Content-Type": 'application/xml; charset=utf-8;',
                "Connection": "keep-alive",
            },
        })
    }
    /**
     * return all events from the api.
     */
    async AllEvents() {
        try {
            const response = await this.axiosInstance.get(this.endpoint, {
                params: {
                    q: this.query,
                    authorization: this.pub,
                    refine: true,
                    sort: "act_dt_asc",
                }
            }).then((response) => {
                // doesnt have to be OK but has to be within the 2xx range. ~Normie
                if(response.status >= 200 <= 300) {
                    // turn xml to json.
                    // edit: forgot to parse it, without parsing the data, it would be a JSON String instead of an Object ~Normie
                    let data = JSON.parse(convertor.xml2json(response.data, {compact: true, spaces: 2}));
                    return data;
                }
            });

            return response;
        } catch (error) {
            console.error(error);
        }

    }
    /**
     * return one event through it's id from the api.
     * @param {Number} id 
     */
    async getEvent(id) {
        this.query += " nativeId:" + id;
        try {
            const response = await this.axiosInstance.get(this.endpoint, {
                params: {
                    q: this.query,
                    authorization: this.pub,
                    refine: true,
                    sort: "act_dt_asc",
                }
            }).then((response) => {
                // doesnt have to be OK but has to be within the 2xx range. ~Normie
                if(response.status >= 200 <= 300) {
                    // turn xml to json.
                    // edit: forgot to parse it, without parsing the data, it would be a JSON String instead of an Object ~Normie
                    let data = JSON.parse(convertor.xml2json(response.data, {compact: true, spaces: 2}));
                    return data;
                }
            });
            return response
        } catch (error) {
            console.error(error);
        }
    }
}

module.exports = OBA;