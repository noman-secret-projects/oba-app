/**
 * DateParser class - parses the date or the time out of an string.
 * 
 * @author Noman Jabbar and Adam Abbas.
 */
class DateParser {

    constructor() {
        this.time;
        this.date;
    }

    /**
     * Checks whether a char is a digit
     *
     * @param char
     * @returns {boolean|boolean}
     */
    isCharDigit(char) {
        return !!char.trim() && char > -1;
    }

    /**
     * this will parse the date and time out of the given event by setting a given property (it's either date or time).
     * @param {String} infoText 
     * @param {String} property
     */
    parse (infoText, property) {
        //the non-relevent info is sliced out.
        let filter = ["maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"];

        // check if the day is present in the given string
        filter.forEach(element => {
            // if this given day is inside the string, split it and return the last substring.
            if (infoText.includes(element)) {
                let dateText = infoText.split(element).pop();
                // trim the whitespace of the substring.
                dateText.trim();
                
                // we split it again on the term "om" so we can get the date and time seperately.
                let date = dateText.split("om").shift();
                let time = dateText.split("om").pop();
                // parses the time out of the string. checks where "uur" occurs and then trims it, lastly it adds the seconds to it.
                this.time = time.substring(0, time.indexOf("uur")).trim() + ":00";

                const monthArray = ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"];

                let day;
                let month;

                for (let i = 0; i < monthArray.length; i++) {
                    const INDEXES = {
                        DOUBLE_DIGIT: 3,
                        SINGLE_DIGIT: 2,
                        NON_EXISTING: -1
                    };
                    const beginIndex = date.indexOf(monthArray[i]);

                    //the beginIndex is -1 if the string doesn't exist. So if it's not -1, it exists.
                    if (beginIndex !== INDEXES.NON_EXISTING) {
                        //slices out the month
                        month = date.slice(beginIndex, beginIndex + monthArray[i].length);

                        //the day is always three or two indexes before the month, depending if the day is a single digit or a double digit.
                        //so if the character three indexes before the month is a digit, then the day is a double digit. Otherwise it's a single digit.
                        if (this.isCharDigit(date.charAt(beginIndex - 3))) {
                            day = date.slice(beginIndex - INDEXES.DOUBLE_DIGIT, beginIndex - 1);
                            console.log(typeof day, day)
                        } else {
                            day = date.slice(beginIndex - INDEXES.SINGLE_DIGIT, beginIndex - 1);
                            console.log(typeof day, day)
                        }
                        break;
                    }
                }
                // string interpolate (I like using these words) the year, month and day into a date object. the plus 1 is for getting the month else you will get the month before.
                let eventDate = `${new Date().getFullYear()}-0${monthArray.indexOf(month) + 1}-${parseInt(day)}`;
                this.date = eventDate;
            }
        });

        switch (property) {
            case "time":
                return this.time;
            
            case "date":
                return this.date;
        }
    }
}
module.exports = DateParser;