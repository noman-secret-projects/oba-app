/**
 * Server application - contains all server config, api endpoints and contains all cron tasks.
 *
 * @author Noman Jabbar, Sam Overheul, Adam Abbas, Ryan Veerman & Sahil Mankani.
 */
// dependencies
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const nodemailer = require('nodemailer');
const cron = require('node-cron');
// API and utils
const db = require("./utils/databaseHelper");
const cryptoHelper = require("./utils/cryptoHelper");
const corsConfig = require("./utils/corsConfigHelper");
const OBA = require("./utils/OBA");
const DateParser = require("./utils/dateParser");
// instances
const app = express();
//init mysql connectionpool
const connectionPool = db.init();
//logger lib  - 'short' is basic logging info
app.use(morgan("short"));
//parsing request bodies from json to javascript objects
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
//CORS config - Cross Origin Requests
app.use(corsConfig);

// CRON tab - every night at 3 am put everything from oba api into db.
cron.schedule('0 3 * * *', () => {

    let obaIds = [];
    let dbIds = [];
    let obaData = {};
    let dateParser = new DateParser();
    let oba = new OBA();

    // retrieve all events from the OBA API
    oba.AllEvents().then((data) => {
        // store it into variable named events.
        let events = data.aquabrowser.results.result;

        console.time('OBA-migrate-DB');
        console.log("===CRON JOB ACTIVATED===")
        console.log("getting all events from OBA...")
        for (key in events) {
            // foreach event push the current event id into the first dataset (it's basically an array).
            obaIds.push(parseInt(events[key].id._attributes.nativeid));
            // obaData is the events that will be stored into the db later on. for now fill it with all events.
            obaData[events[key].id._attributes.nativeid] = {
                title: events[key].titles.title._text,
                info: events[key]['course-infos']['course-info']._text,
            }
        }
        // this will sort the ids from lowest to highest number.
        obaIds.sort((a, b) => {
            return a - b;
        })
        console.log("getting all events from DB...")
        // get all "events" that are currently in the table events.
        db.handleQuery(connectionPool, {
            query: "SELECT id from event",
        }, (data) => {
            if (data) {
                data.forEach(event => {
                    // foreach event id in the db push the current id into the second dataset (it's basically an array).
                    dbIds.push(event.id);
                });
                // like the first dataset, sort it from lowest to highest number.
                dbIds.sort((a, b) => {
                    return a - b;
                });
            }
            // contains the ids that are in each set. which means they are migrated successfully.
            let existingIds = obaIds.filter(id => dbIds.includes(id))
            // contains the ids that isnt in the oba events dataset and but still are inside the db.
            let redundantIds = dbIds.filter((id) => !existingIds.includes(id));
            // contains the ids that are in the oba events dataset and are not inside the db.
            let newIds = obaIds.filter(id => !dbIds.includes(id))
            if (redundantIds.length > 0) {
                console.log("found redundant ids: removing them from DB...")
                // this will remove all ids that are not present in the OBA events dataset.
                redundantIds.forEach(id => {
                    now = new Date();
                    db.handleQuery(connectionPool, {
                        query: "UPDATE event SET deleted_at = ? WHERE id = ?",
                        values: [`${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`, id],
                    }, (data) => {
                    }, (err) => {
                    })
                });
            } else {
                console.log("no redundant events found.")
            }

            if (newIds.length > 0) {
                console.log("found new ids: inserting them into DB...")
                // this will insert all ids that are present in the OBA events dataset but are not in the db.
                newIds.forEach((id) => {
                    // extract the date from the string with the custom made dateParser class.
                    let date = dateParser.parse(obaData[id].info, "date");
                    // extract the time from the string with the custom made dateParser class.
                    let time = dateParser.parse(obaData[id].info, "time");

                    db.handleQuery(connectionPool, {
                        query: "INSERT INTO event (id, title, date, time) VALUES(?, ?, ?, ?)",
                        values: [id, obaData[id].title, date, time],
                    }, (data) => { // do nothing when this succeeds.
                    }, (err) => { // do nothing when this fails.
                    })
                })
            } else {
                console.log("no new events found.")
            }
        }, (err) => {
        })
        console.timeEnd('OBA-migrate-DB');
    });
});
// ------ ROUTES - add all api endpoints here ------
const httpOkCode = 200;
const badRequestCode = 400;
const authorizationErrCode = 401;
// ------ USERS ------
app.post("/user", (req, res) => {
    let email = req.body.email;

    db.handleQuery(connectionPool, {
        query: "SELECT email FROM user WHERE email = ?",
        values: [email]
    }, (data) => {
        res.status(httpOkCode).json({email: data[0].email});
    }, (err) => res.status(badRequestCode).json({reason: err}));
});

app.post("/user/send", (req, res) => {

    // creating the data for the url
    const email = req.body.email;
    let hash = cryptoHelper.getHashedPassword(email);

    // transporter, which means this will send the email for us to the targeted email.
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        secure: false,
        auth: {
            // test gmail account to send emails from.
            user: process.env.MAIL_NAME,
            pass: process.env.MAIL_PASS
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    // get id from the user.
    db.handleQuery(connectionPool, {
        query: "SELECT id FROM user WHERE email = ?",
        values: [email]
    }, (data) => {
        if (data.length === 1) {
            // @DEV
            // the hash is the email hashed.
            const link = `https://dev-oba-5.hbo-ict.cloud/?hash=${hash}&uid=${data[0].id}#password_recovery`;

            // format the email.
            let MailText = {
                from: '"OBA NoReply" <OBA.Wachtwoord@gmail.com',
                to: email,
                subject: 'Wachtwoord vergeten',
                html: '<p>Klik <a href="' + link + '"\>hier</a> om je wachtwoord te wijzigen.</p>' +
                    '<br><br><br>' +
                    '<p>Als jij dit hebt gedaan, kun je deze e-mail negeren.</p>'
            };
            // send the email.
            transporter.sendMail(MailText, (error, info) => {
                if (error) {
                    console.log(error);
                } else {
                    res.status(httpOkCode).json({"mail": email});
                }
            })
        } else {
            //wrong email
            res.status(authorizationErrCode).json({reason: "Verkeerde email."});
        }
    }, (err) => res.status(badRequestCode).json({reason: err}));
});

app.post("/user/login", (req, res) => {
    const email = req.body.email;
    const password = cryptoHelper.getHashedPassword(req.body.password);

    db.handleQuery(connectionPool, {
        query: "SELECT email, password FROM user WHERE email = ? AND password = ?",
        values: [email, password]
    }, (data) => {
        if (data.length === 1) {
            //return just the email for now, never send password back!
            res.status(httpOkCode).json({"email": data[0].email});
        } else {
            //wrong email or password
            res.status(authorizationErrCode).json({reason: "Verkeerd email of wachtwoord"});
        }
    }, (err) => res.status(badRequestCode).json({reason: err}));
});

app.post("/user/register", (req, res) => {

    let email = req.body.email;
    let password = cryptoHelper.getHashedPassword(req.body.password);

    db.handleQuery(connectionPool, {
        query: "INSERT INTO user(email, password) VALUES(?, ?)",
        values: [email, password]
    }, (data) => {
        // if succeeded, send HTTP 200 back.
        res.status(httpOkCode).json({status: httpOkCode});
    }, (err) => res.status(badRequestCode).json({reason: err}));
});

app.post("/user/update", (req, res) => {
    let email = req.body.oldemail;
    let newEmail = req.body.newemail;

    db.handleQuery(connectionPool, {
        query: "UPDATE user SET email = ? WHERE email = ?",
        values: [newEmail, email]
    }, () => {
        res.status(httpOkCode).json({code: httpOkCode, email: newEmail});
    }, (err) => {
        res.status(httpOkCode).json({code: badRequestCode, reason: err});
    });
});

app.post("/user/update/password", (req, res) => {
    let email = req.body.email;
    let newPassword = cryptoHelper.getHashedPassword(req.body.password);

    db.handleQuery(connectionPool, {
        query: "UPDATE user SET password = ? WHERE email = ?",
        values: [newPassword, email]
    }, () => {
        res.status(httpOkCode).json({email: email});

    }, (err) => {
        res.status(badRequestCode).json({reason: err});
    });
});

app.post("/user/verify/mail", (req, res) => {
    let hash = req.body.hash;

    db.handleQuery(connectionPool, {
            query: "SELECT email FROM user WHERE id = ?",
            values: [req.body.id]
        }, (data) => {
            if (cryptoHelper.getHashedPassword(data[0].email) === hash.split(' ').join('+')) {
                res.status(httpOkCode).json({status: httpOkCode, email: data[0].email});
            } else {
                res.status(httpOkCode).json({status: badRequestCode, message: "Er ging iets mis..."})
            }
            //just give all data back as json
        }, (err) => res.status(badRequestCode).json({reason: err})
    );

});
// ------ EVENTS ------
app.post("/event", (req, res) => {
    db.handleQuery(connectionPool, {
        query: "SELECT title, amount FROM event WHERE id = ?",
        values: [req.body.eventId]
    }, (data) => {

        if (data.length === 1) {
            res.status(httpOkCode).json({name: data[0].title, amount: data[0].amount});
        } else {
            res.status(badRequestCode).json({reason: 'event id does not exist'});
        }
    }, (err) => {
        res.status(badRequestCode).json({reason: err});
    })
});

app.post("/event/all", (req, res) => {
    let oba = new OBA();

    oba.AllEvents().then((data) => {

        let eventsData = {};
        let ids = [];
        let events = data.aquabrowser.results.result;

        for (let key in events) {

            let tutors = events[key]['course-tutors']['course-tutor'];
            let tutor = "";
            // push the event id from the current event into an array.
            ids.push(parseInt(events[key].id._attributes.nativeid));
            // when there are multiple tutors, stringify them. else tutor is just the tutor from the current event.
            if (tutors.length !== undefined) {
                tutor = tutors[0]._text + ", " + tutors[1]._text;
            } else {
                tutor = tutors._text;
            }
            // format the eventsData so that we dont have to send everything to the frontend.
            eventsData[events[key].id._attributes.nativeid] = {
                id: events[key].id._attributes.nativeid,
                title: events[key].titles.title._text,
                subject: events[key].subjects['topical-subject']._attributes['search-term'],
                info: events[key]['course-infos']['course-info']._text,
                price: events[key]['course-prices']['course-price']._text,
                tutor: tutor,
                location: events[key]['course-locations']['course-location']._text
            }
        }
        // get all ids, dates and times from the events table.
        db.handleQuery(connectionPool, {
            query: "SELECT id, amount, date, time FROM event"
        }, (data) => {
            data.forEach(event => {
                // foreach dataPacketRow from the db, check if the current id corresponds with the event id in the array, if it does add the db data to the eventsData.
                if (ids.includes(event.id)) {
                    Object.assign(eventsData[event.id], {amount: event.amount, date: event.date, time: event.time});
                }
            });
            // eventually send the formatted eventsData to the frontend.
            return res.status(httpOkCode).send(eventsData);
        })
    })
});

app.post("/event/amount", (req, res) => {
    db.handleQuery(connectionPool, {
        query: "SELECT amount FROM event WHERE id = ?",
        values: req.body.eventId
    }, (data) => {
        res.status(httpOkCode).json({amount: data[0].amount})
    }, (err) => {
        res.status(badRequestCode).json({reason: err});
    });
});

app.post("/event/update/amount", (req, res) => {
    db.handleQuery(connectionPool, {
        query: "UPDATE event SET amount = ? WHERE id = ?",
        values: [req.body.newAmount, req.body.eventId]
    }, () => {
        res.status(httpOkCode).json({amount: req.body.newAmount})
    }, (err) => {
        res.status(badRequestCode).json({reason: err})
    });
});

app.post("/event/export", (req, res) => {
    db.handleQuery(connectionPool, {
            query: "SELECT id, title, amount, date, time FROM `event`",
        }, (data) => {
            if (data != null) {
                res.status(httpOkCode).json({data: data})
            }
        }, (err) => res.status(badRequestCode).json({reason: err})
    )
});

app.post("/event/total", (req, res) => {

    db.handleQuery(connectionPool, {
        query: "SELECT COUNT(*) AS total FROM event",
    }, (data) => {

        res.status(httpOkCode).json({"output": data[0].total})
    }, (err) => {})
});
app.post("/event/total/amount", (req, res) => {

    db.handleQuery(connectionPool, {
        query: "SELECT SUM(amount) AS total FROM event",
    }, (data) => {

        res.status(httpOkCode).json({"output": data[0].total})
    }, (err) => {})
});

app.post("/event/today", (req, res) => {
    
    db.handleQuery(connectionPool, {
        query: "SELECT title FROM event WHERE date = ?",
        values: [req.body.date]
    }, (data) => {
        try {
            res.status(httpOkCode).json({"title": data[0].title})
        }
        catch(e) {
            res.status(httpOkCode).json({"title": "Geen evenementen vandaag!"})
        }
    }, (err) => {})
});

app.post("/event/recent", (req, res) => {
    
    db.handleQuery(connectionPool, {
        query: "SELECT date, time, title, id, amount FROM `event` ORDER BY date DESC LIMIT 7",
    }, (data) => {
        res.status(httpOkCode).json({"output": data})
    }, (err) => {})
});
// ------ ROOMS ------
app.post("/room/set/tag", (req, res) => {

    let roomId = req.body.roomId;
    let timestamp = req.body.timestamp;
    let eventId = req.body.eventId;

    db.handleQuery(connectionPool, {
            query: "INSERT INTO tags (room_id, valid_until, event_id) VALUES (?, ?, ?)",
            values: [roomId, timestamp, eventId]
        }, (data) => {
            console.log(data);
        }, (err) => res.status(badRequestCode).json({reason: err})
    )
});

app.post("/room/get/eventId", (req, res) => {

    let roomId = req.body.roomId;
    let timestamp = req.body.timestamp;

    db.handleQuery(connectionPool, {
            query: "SELECT event_id FROM tags WHERE tags.valid_until > ? AND room_id = ? ORDER BY valid_until DESC",
            values: [timestamp, roomId]
        }, (data) => {
            res.status(httpOkCode).json({"eventId": data[0].event_id});
        }, (err) => res.status(badRequestCode).json({reason: err})
    )
});

app.post("/room/set/eventId", (req, res) => {

    let eventId = req.body.eventId;

    db.handleQuery(connectionPool, {
            query: "UPDATE event SET amount = amount+1 WHERE id = ?",
            values: [eventId]
        }, (data) => {
            console.log(data);
        }, (err) => res.status(badRequestCode).json({reason: err})
    )
});
//------- END ROUTES -------
module.exports = app;